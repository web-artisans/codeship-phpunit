FROM phpunit/phpunit:5.4.7

RUN apk update && apk add -y mysql-client

RUN apk --no-cache --repository http://dl-cdn.alpinelinux.org/alpine/edge/testing add php7-session

ENTRYPOINT []